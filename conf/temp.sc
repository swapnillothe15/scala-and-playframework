val a:Int = 4

implicit class RichInt(int: Int){
  def design ={
    s"======= $int =========="
  }

  def double={
    int*2
  }
}

print(a.design)
print(a.double)
