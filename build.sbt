name := """scala-app"""
organization := "com.example"
ThisBuild / version := "1.0-SNAPSHOT"
ThisBuild / scalaVersion := "2.12.6"
val rootDirectory = file(".")


lazy val root = (project in file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq("org.flywaydb" % "flyway-core" % "4.2.0", guice)
libraryDependencies += jdbc
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test
libraryDependencies ++= Seq(
  "com.zaxxer" % "HikariCP" % "2.7.9",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.3"
) ++ Seq(
  "org.postgresql" % "postgresql" % "9.4.1212"
) ++ Seq(
  "com.zaxxer" % "HikariCP" % "2.7.9",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.3"
) ++ Seq(
  "com.typesafe.slick" %% "slick" % "3.2.3",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.3.0",
  "org.joda" % "joda-convert" % "2.0.1"
) ++ Seq(
  "org.scala-lang.modules" %% "scala-async" % "0.9.7"
) ++ Seq(
  "org.scala-lang.modules" %% "scala-async" % "0.9.7"
)



libraryDependencies += "org.mockito" % "mockito-core" % "2.18.3" % "test"

PlayKeys.devSettings += "play.server.http.port" -> "9000"
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.example.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.example.binders._"
