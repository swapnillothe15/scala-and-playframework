FROM openjdk:8-jre

COPY svc /svc

EXPOSE 9000

ENV APP_SECRET_PRICING 'QCY?tAnfk?aZ?iwrNwnxIlR6CTf:G3gf:90Latabg@5241AB`R5W:1uDFN];Ik@n'

CMD /svc/bin/start -Dplay.http.secret.key=${APP_SECRET_PRICING}
