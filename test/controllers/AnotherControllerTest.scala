package controllers

import Repository.ArticleRepository
import domain.{ImplicitReqArg, Random}
import org.scalatestplus.play._
import play.api.Configuration
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.ExecutionContext.Implicits.global

class AnotherControllerTest extends PlaySpec {

  "AnotherControllerTest" should {
    "index" in {
      val conf: Configuration = Configuration()

      val controller = new AnotherController(
        stubControllerComponents(),
        conf, new Random, new ImplicitReqArg,
        new ArticleRepository()
      )

      val response = controller.index().apply(FakeRequest(GET, "/index"))

      status(response) mustBe OK
      contentAsString(response) must include("indexPage")
    }

  }
}
