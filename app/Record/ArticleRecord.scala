package Record

import play.api.libs.json.{Json, OFormat}

case class ArticleRecord(
                          productKey: Int,
                          name: String
                        )

object ArticleRecord {
  implicit val f: OFormat[ArticleRecord] = Json.format[ArticleRecord]
}
