package domain

import play.api.mvc._

class ImplicitReqArg {
  def get(implicit request: Request[AnyContent]): String ={
    s"This is via through implicit $request"
  }
}
