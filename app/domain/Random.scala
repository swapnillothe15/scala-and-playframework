package domain

class Random {
  def get: Long = {
    Math.round(Math.random() * 10 + 1)
  }
}
