package db

import com.zaxxer.hikari.HikariDataSource
import slick.jdbc.PostgresProfile.api._

object PostgresContext {
  def getDatabase = {
    val config = DBMigration.hikariConfig
    Database.forDataSource(
      ds = new HikariDataSource(config),
      maxConnections = Some(config.getMaximumPoolSize),
      executor = AsyncExecutor(
        name = "SomeAsyncExecutor",
        minThreads = config.getMaximumPoolSize,
        maxThreads = config.getMaximumPoolSize,
        queueSize = 1000,
        maxConnections = config.getMaximumPoolSize
      )
    )

  }
}
