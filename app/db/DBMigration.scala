package db

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import javax.inject.Inject
import org.flywaydb.core.Flyway
import play.api.db.ConnectionPool
import play.api.{Logger, Mode}
import play.utils.UriEncoding

class DBMigration @Inject()(wrappedConfiguration: WrappedConfiguration) {

  processMigration()

  def getHikariConfigFor(dbUrl: String): HikariConfig = {

    val hikariConfig = new HikariConfig()

    val (url, userAndPass) = ConnectionPool.extractUrl(Some(dbUrl), Mode.Prod)
    val (username, password) = userAndPass.get
    hikariConfig.setJdbcUrl(url.get)
    hikariConfig.setUsername(username)
    hikariConfig.setPassword(UriEncoding.decodePath(password, "UTF8"))
    hikariConfig.setDriverClassName(wrappedConfiguration.get[String]("dbConfig.driver"))
    hikariConfig.setMaximumPoolSize(wrappedConfiguration.get[Int]("dbConfig.connectionPool"))
    hikariConfig.setConnectionTimeout(wrappedConfiguration.get[Int]("dbConfig.connectionTimeOut"))
    hikariConfig
  }

  private def migrate(source: HikariDataSource) = {
    val flyway: Flyway = new Flyway()
    flyway.setDataSource(source)
    flyway.setLocations(wrappedConfiguration.get[String]("dbmigration.location"))
    val schema = wrappedConfiguration.get[String]("dbmigration.schema")
    flyway.setSchemas(schema)
    flyway.migrate()
  }

  private def processMigration(): Unit = {
    val dbUrl = wrappedConfiguration.get[String]("database.url")
    Logger.info(s"Migrating db config")
    DBMigration.hikariConfig = getHikariConfigFor(dbUrl)
    try {
      val datasource = new HikariDataSource(DBMigration.hikariConfig)
      migrate(datasource)
      datasource.close()
    }
    catch {
      case e: Exception =>
        Logger.error(s"Error while closing connection pool for DB migration", e)
        throw e
    }
  }

  private def countryCode(key: String) = {
    key.substring(0, key.indexOf("."))
  }
}

object DBMigration {
  var hikariConfig: HikariConfig = _
}
