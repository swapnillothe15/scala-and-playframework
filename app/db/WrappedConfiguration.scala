package db

import javax.inject.{Inject, Singleton}
import play.api.{ConfigLoader, Configuration}

@Singleton
class WrappedConfiguration @Inject()(
                                      configuration: Configuration
                                    ) {

  def keys(): Set[String] = {
    configuration.keys
  }

  def get[A](path: String)
            (implicit loader: ConfigLoader[A]): A = {
    configuration.get[A](path)
  }

  def getOptional[A](path: String)
                    (implicit loader: ConfigLoader[A]): Option[A] = {
    configuration.getOptional[A](path)
  }

  def getForCountry[A](path: String, fallbackToDefault: Boolean)
                      (implicit loader: ConfigLoader[A]): A = {
    configuration.getOptional[A](s"${""}.$path") match {
      case Some(value)                =>
        value
      case None if fallbackToDefault  =>
        configuration.get[A](path)
      case None if !fallbackToDefault =>
        throw new RuntimeException(s"${""}.$path key not found in configuration")
    }
  }

  def getForCountryOptional[A](path: String)(implicit loader: ConfigLoader[A]): Option[A] = {
    configuration.getOptional[A](s"${""}.$path") match {
      case Some(config) =>
        Some(config)
      case None         =>
        configuration.getOptional[A](path)
    }
  }
}
