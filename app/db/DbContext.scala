package db

import slick.jdbc.PostgresProfile

trait DbContext {
  def dbContext[T](block: PostgresProfile.backend.DatabaseDef => T): T = {
    val database = PostgresContext.getDatabase
    block(database)
  }
}
