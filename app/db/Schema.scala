package db

import Table.ArticleTable
import slick.lifted.TableQuery

object Schema {
  val articles: TableQuery[ArticleTable] = TableQuery[ArticleTable]
}
