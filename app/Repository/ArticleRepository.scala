package Repository

import Record.ArticleRecord
import db.{DbContext, Schema}
import javax.inject.{Inject, Singleton}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ArticleRepository @Inject()(implicit ec: ExecutionContext) extends DbContext {
  def insert(productKey: Int, name: String): Future[Int] = {
    dbContext(_.run(Schema.articles.insertOrUpdate(ArticleRecord(productKey, name))))
  }

  def articles: Future[Seq[ArticleRecord]] = {
    dbContext(_.run(Schema.articles.result))
  }
}
