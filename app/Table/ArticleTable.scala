package Table


import Record.ArticleRecord
import slick.jdbc.PostgresProfile.api._

class ArticleTable(tag: Tag) extends Table[ArticleRecord](tag, "articles") {

  def productKey = column[Int]("product_key")

  def name = column[String]("name", O.PrimaryKey)

  def * = (productKey, name).shaped <> ((ArticleRecord.apply _).tupled, ArticleRecord.unapply)
}
