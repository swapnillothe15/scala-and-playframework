import com.google.inject.AbstractModule
import db.DBMigration
import play.api.libs.concurrent.AkkaGuiceSupport

class AppModule extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind(classOf[DBMigration]).asEagerSingleton()
  }
}
