package controllers

import Repository.ArticleRepository
import domain.{ImplicitReqArg, Random}
import javax.inject.Inject
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc._

import scala.async.Async.{async, await}
import scala.concurrent.ExecutionContext.Implicits.global

class AnotherController @Inject()(
                                   cc: ControllerComponents,
                                   conf: Configuration,
                                   random: Random,
                                   implicitReqArg: ImplicitReqArg,
                                   articleRepository: ArticleRepository
                                 ) extends AbstractController(cc) {

  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok("indexPage")
  }

  def greet(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.fromsecondcontroller("Have a great day!"))
  }

  def todo(): Action[AnyContent] = TODO

  def rand: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(s"This is random number : ${random.get}")
  }

  def configName: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(s"This is config name : ${conf.get[String]("default.name")}")
  }

  def getImplicitReq: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(implicitReqArg.get)
  }

  def getEmployeeFor(id: Int): Action[AnyContent] = Action {
    Ok(s"=======id_$id==========")
  }

  def getIdFor(name: String, id: Int): Action[AnyContent] = Action {
    Ok(s"=======id for $name is 123==========")
  }

  def articles: Action[AnyContent] = Action.async {
    async {
      val articles = await(articleRepository.articles)
      Ok(Json.toJson(articles))
    }
  }

  def insert: Action[AnyContent] = Action.async {
    async {
      val articleCount = await(articleRepository.articles).size + 1
      val resultingUsers = await(articleRepository.insert(articleCount, s"$articleCount article"))
      Ok("ok" + resultingUsers.toString)
    }
  }
}
