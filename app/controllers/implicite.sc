def bar(b: String)(implicit a: Int) = {
  a
}

def foo(implicit a: Int) = {
  bar("bar")
}


case class PI(value: Double)

implicit def piConverter(d: Double) = PI(d)

implicit val _pi: PI = 3.14

implicit val someVariable: Double = 5.0

def area(r: Int)(implicit pi: PI) = {
  pi.value * r * r
}

def perimeter(r: Int)(implicit pi: PI) = {
  2 * pi.value * r
}

area(10)

val a: Integer = 11

implicit class RichInteger(n: Integer) {
  def isEven: Boolean = n % 2 == 0
}

a.isEven


val string :String = "stringvariable"

implicit class RichString(string:String){
  def < (otherString:String)={
    string.length < otherString.length
  }
}

string.{
  "ohh"
}
