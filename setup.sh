#!/usr/bin/env bash
rm -rf target svc
sbt dist
set -x
unzip -d svc target/universal/*-1.0-SNAPSHOT.zip
mv svc/*/* svc/
rm svc/bin/*.bat
mv svc/bin/* svc/bin/start
docker build -t swapnillothe15/scala-app .
#docker run -it -d -p 9000:9000 swapnillothe15/scala-app
